class Module:

    def __init__(self, name, buildtool):
        self.name = name
        self._buildtool = buildtool
        self.sources = []
        self.depends = []
        self.variables = {}
        self.config = {}

    def buildtool(self):
        return self._buildtool
