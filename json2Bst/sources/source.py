class Source:

    def __init__(self, kind):
        self._kind = kind
        self.attributes = {}
        self.incompatable = []

    def kind(self):
        return self._kind
